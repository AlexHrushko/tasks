import React from "react";
import "./App.css";
import Container from "react-bootstrap/Container";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LoginContainer from "../Login/LoginContainer";
import Home from "../Home/Home";

function App() {
  // return <Home />;
  return (
    <Router>
      <Container className="mt-5 mb-5">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <LoginContainer />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
