import React, { Component } from "react";
import PropTypes from "prop-types";
import Alert from "react-bootstrap/Alert";

class Notification extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    setTimeout(() => {
      this.props.setShowNotification(false);
    }, 3000);
  }

  render() {
    return (
      <Alert
        show={this.props.showNotification}
        variant="success"
        dismissible={true}
        onClose={() => this.props.setShowNotification(false)}
      >
        Новая задача добавлена успешно!
      </Alert>
    );
  }
}

Notification.propTypes = {
  showNotification: PropTypes.bool.isRequired,
  setShowNotification: PropTypes.func.isRequired
};

export default Notification;
