import { connect } from "react-redux";
import Notification from "./Notification";
import { SET_SHOW_NOTIFICATION } from "../../store/actions/actionTypes";

const mapStateToProps = state => ({
  showNotification: state.tasks.showNotification
});

const mapDispatchToProps = dispatch => ({
  setShowNotification: value => dispatch({ type: SET_SHOW_NOTIFICATION, value })
});

const NotificationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Notification);

export default NotificationContainer;
