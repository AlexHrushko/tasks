import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import { Button } from "react-bootstrap";
import Col from "react-bootstrap/Col";

class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.task.text,
      validate: false
    };

    this.onChangeStatus = this.onChangeStatus.bind(this);
    this.onSaveText = this.onSaveText.bind(this);
  }

  onSaveText(event) {
    event.preventDefault();
    this.props.editTask({
      id: this.props.task.id,
      text: this.state.text,
      status: this.props.task.status
    });
  }

  onChangeStatus(event) {
    this.props.editTask({
      id: this.props.task.id,
      text: this.props.task.text,
      status: event.target.checked ? 10 : 0
    });
  }

  render() {
    if (this.props.isLoggedIn) {
      return (
        <tr>
          <td>{this.props.task.username}</td>
          <td>{this.props.task.email}</td>
          <td>
            <Form onSubmit={this.onSaveText}>
              <Form.Row>
                <Form.Group as={Col} md="9">
                  <Form.Control
                    required
                    type="text"
                    value={this.state.text}
                    onChange={event =>
                      this.setState({ text: event.target.value })
                    }
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Button type="submit" variant="success">
                    Сохранить
                  </Button>
                </Form.Group>
              </Form.Row>
            </Form>
          </td>
          <td>
            <Form.Check
              label="Выполено"
              type="checkbox"
              checked={this.props.task.status === 10}
              onChange={this.onChangeStatus}
            />
          </td>
        </tr>
      );
    } else {
      return (
        <tr>
          <td>{this.props.task.username}</td>
          <td>{this.props.task.email}</td>
          <td>{this.props.task.text}</td>
          <td>{this.props.task.status === 10 ? "Выполено" : "Не выполено"}</td>
        </tr>
      );
    }
  }
}

Task.propTypes = {
  task: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      username: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      status: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  editTask: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired
};

export default Task;
