import { connect } from "react-redux";
import { editTask } from "../../store/actions";
import Task from "./Task";

const mapStateToProps = (state, props) => ({
  task: props.task,
  isLoggedIn: state.tasks.isLoggedIn
});

const mapDispatchToProps = dispatch => ({
  editTask: editTask(dispatch)
});

const TaskContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Task);

export default TaskContainer;
