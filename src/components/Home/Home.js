import React from "react";
import TasksListContainer from "../TasksList/TasksListContainer";
import CreateTaskContainer from "../CreateTask/CreateTaskContainer";
import NotificationContainer from "../Notification/NotificationContainer";
import TasksPaginationContainer from "../TasksPagination/TasksPaginationContainer";
import LoginLogoutButtonContainer from "../LoginLogoutButton/LoginLogoutButtonContainer";

function Home() {
  return (
    <div>
      <LoginLogoutButtonContainer />
      <NotificationContainer />
      <TasksListContainer />
      <TasksPaginationContainer />
      <CreateTaskContainer />
    </div>
  );
}

export default Home;
