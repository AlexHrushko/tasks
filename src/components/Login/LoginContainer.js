import { connect } from "react-redux";
import { login } from "../../store/actions";
import Login from "./Login";

const mapStateToProps = state => ({
  isLoggedIn: state.tasks.isLoggedIn,
  loginError: state.tasks.loginError
});

const mapDispatchToProps = dispatch => ({
  login: login(dispatch)
});

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default LoginContainer;
