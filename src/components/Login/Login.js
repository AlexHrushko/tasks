import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { Redirect } from "react-router-dom";
import Alert from "react-bootstrap/Alert";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeUsername(event) {
    this.setState({
      ...this.state,
      username: event.target.value
    });
  }

  onChangePassword(event) {
    this.setState({
      ...this.state,
      password: event.target.value
    });
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.login({
      username: this.state.username,
      password: this.state.password
    });
  }

  render() {
    if (this.props.isLoggedIn) {
      return <Redirect to="/" />;
    }

    return (
      <Container>
        <Alert show={this.props.loginError} variant="danger">
          Неверный логин или пароль.
        </Alert>
        <Row className="justify-content-center mt-3">
          <Form onSubmit={this.onSubmit}>
            <Form.Row>
              <Form.Group>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  style={{
                    width: 300
                  }}
                  required
                  type="text"
                  value={this.state.username}
                  onChange={this.onChangeUsername}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  style={{
                    width: 300
                  }}
                  required
                  type="password"
                  value={this.state.password}
                  onChange={this.onChangePassword}
                />
              </Form.Group>
            </Form.Row>
            <Button type="submit">Sign in</Button>
          </Form>
        </Row>
      </Container>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  loginError: PropTypes.bool.isRequired
};

export default Login;
