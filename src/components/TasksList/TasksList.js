import React, { Component } from "react";
import PropTypes from "prop-types";
import Table from "react-bootstrap/Table";
import TaskContainer from "../Task/TaskContainer";

class TasksList extends Component {
  constructor(props) {
    super(props);

    this.onSortName = this.onSortName.bind(this);
    this.onSortEmail = this.onSortEmail.bind(this);
    this.onSortStatus = this.onSortStatus.bind(this);
  }

  componentDidMount() {
    this.props.getTasks({
      page: 1
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.updateList) {
      this.props.getTasks({
        page: this.props.page,
        sortField: this.props.sortField,
        sortDirection: this.props.sortDirection
      });
    }
  }

  onSortName() {
    this.props.setSortField("name");
    if (!this.props.sortField || this.props.sortField !== "name") {
      this.props.setSortDirection("asc");
    } else {
      if (this.props.sortDirection === "asc") {
        this.props.setSortDirection("desc");
      } else {
        this.props.setSortDirection("asc");
      }
    }
  }

  onSortEmail() {
    this.props.setSortField("email");
    if (!this.props.sortField || this.props.sortField !== "email") {
      this.props.setSortDirection("asc");
    } else {
      if (this.props.sortDirection === "asc") {
        this.props.setSortDirection("desc");
      } else {
        this.props.setSortDirection("asc");
      }
    }
  }

  onSortStatus() {
    this.props.setSortField("status");
    if (!this.props.sortField || this.props.sortField !== "status") {
      this.props.setSortDirection("asc");
    } else {
      if (this.props.sortDirection === "asc") {
        this.props.setSortDirection("desc");
      } else {
        this.props.setSortDirection("asc");
      }
    }
  }

  render() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th onClick={this.onSortName}>Name</th>
            <th onClick={this.onSortEmail}>Email</th>
            <th>Text</th>
            <th onClick={this.onSortStatus}>Status</th>
          </tr>
        </thead>
        <tbody>
          {this.props.tasks &&
            this.props.tasks.map((task, index) => {
              return <TaskContainer task={task} key={index} />;
            })}
        </tbody>
      </Table>
    );
  }
}

TasksList.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      username: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      status: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  page: PropTypes.number.isRequired,
  getTasks: PropTypes.func.isRequired,
  updateList: PropTypes.bool.isRequired,
  sortField: PropTypes.string,
  sortDirection: PropTypes.string,
  setSortField: PropTypes.func.isRequired,
  setSortDirection: PropTypes.func.isRequired
};

export default TasksList;
