import { connect } from "react-redux";
import { getTasks } from "../../store/actions";
import TasksList from "./TasksList";
import {
  SET_SORT_DIRECTION,
  SET_SORT_FIELD
} from "../../store/actions/actionTypes";

const mapStateToProps = state => ({
  tasks: state.tasks.tasks,
  page: state.tasks.page,
  updateList: state.tasks.updateList,
  sortField: state.tasks.sortField,
  sortDirection: state.tasks.sortDirection
});

const mapDispatchToProps = dispatch => ({
  getTasks: getTasks(dispatch),
  setSortField: value => dispatch({ type: SET_SORT_FIELD, value }),
  setSortDirection: value => dispatch({ type: SET_SORT_DIRECTION, value })
});

const TasksListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksList);

export default TasksListContainer;
