import { connect } from "react-redux";
import TasksPagination from "./TasksPagination";
import { SET_PAGE } from "../../store/actions/actionTypes";

const mapStateToProps = state => ({
  page: state.tasks.page,
  totalTaskCount: state.tasks.totalTaskCount
});

const mapDispatchToProps = dispatch => ({
  setPage: value => dispatch({ type: SET_PAGE, value })
});

const TasksPaginationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksPagination);

export default TasksPaginationContainer;
