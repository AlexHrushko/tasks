import React, { Component } from "react";
import PropTypes from "prop-types";
import Pagination from "react-bootstrap/Pagination";

class TasksPagination extends Component {
  constructor(props) {
    super(props);

    this.goToLastPage = this.goToLastPage.bind(this);
    this.disableNextButton = this.disableNextButton.bind(this);
  }

  goToLastPage() {
    const lastPage = Math.ceil(this.props.totalTaskCount / 3);
    this.props.setPage(lastPage);
  }

  disableNextButton() {
    const lastPage = Math.ceil(this.props.totalTaskCount / 3);
    return this.props.page === lastPage;
  }

  render() {
    return (
      <Pagination>
        <Pagination.First
          disabled={this.props.page === 1}
          onClick={() => this.props.setPage(1)}
        />
        <Pagination.Prev
          disabled={this.props.page === 1}
          onClick={() => this.props.setPage(this.props.page - 1)}
        />
        <Pagination.Item>{this.props.page}</Pagination.Item>
        <Pagination.Next
          disabled={this.disableNextButton()}
          onClick={() => this.props.setPage(this.props.page + 1)}
        />
        <Pagination.Last
          disabled={this.disableNextButton()}
          onClick={this.goToLastPage}
        />
      </Pagination>
    );
  }
}

TasksPagination.propTypes = {
  page: PropTypes.number.isRequired,
  totalTaskCount: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired
};

export default TasksPagination;
