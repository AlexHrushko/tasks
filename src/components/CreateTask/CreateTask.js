import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";

class CreateTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      text: ""
    };

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onChangeUsername(event) {
    this.setState({
      ...this.state,
      username: event.target.value
    });
  }

  onChangeEmail(event) {
    this.setState({
      ...this.state,
      email: event.target.value
    });
  }

  onChangeText(event) {
    this.setState({
      ...this.state,
      text: event.target.value
    });
  }

  onSave(event) {
    event.preventDefault();
    this.setState({
      username: '',
      email: '',
      text: ''
    });
    this.props.createTask({
      username: this.state.username,
      email: this.state.email,
      text: this.state.text
    });
  }

  render() {
    return (
      <Form onSubmit={this.onSave}>
        <Form.Row>
          <Form.Group as={Col} md="4">
            <Form.Label>Username</Form.Label>
            <Form.Control
              required
              type="text"
              value={this.state.username}
              onChange={this.onChangeUsername}
            />
          </Form.Group>
          <Form.Group as={Col} md="4">
            <Form.Label>Email</Form.Label>
            <Form.Control
              required
              type="email"
              value={this.state.email}
              onChange={this.onChangeEmail}
            />
          </Form.Group>
          <Form.Group as={Col} md="4">
            <Form.Label>Text</Form.Label>
            <Form.Control
              required
              type="text"
              value={this.state.text}
              onChange={this.onChangeText}
            />
          </Form.Group>
        </Form.Row>
        <Button variant="success" type="submit">
          Сохранить
        </Button>
      </Form>
    );
  }
}

CreateTask.propTypes = {
  createTask: PropTypes.func.isRequired
};

export default CreateTask;
