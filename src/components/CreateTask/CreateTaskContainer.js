import { connect } from "react-redux";
import {createTask} from "../../store/actions";
import CreateTask from "./CreateTask";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  createTask: createTask(dispatch)
});

const CreateTaskContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTask);

export default CreateTaskContainer;
