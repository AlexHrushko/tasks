import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "react-bootstrap/Button";
import { Link, Redirect } from "react-router-dom";

class LoginLogoutButton extends Component {
  render() {
    if (this.props.editError) {
      return <Redirect to="/login" />;
    }
    if (!this.props.isLoggedIn) {
      return (
        <Link to={"/login"}>
          <Button className="mb-3">Login</Button>
        </Link>
      );
    }
    return (
      <Button className="mb-3" onClick={this.props.logout}>
        Logout
      </Button>
    );
  }
}

LoginLogoutButton.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
  editError: PropTypes.bool.isRequired
};

export default LoginLogoutButton;
