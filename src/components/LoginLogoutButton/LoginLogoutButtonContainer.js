import { connect } from "react-redux";
import { LOGOUT } from "../../store/actions/actionTypes";
import LoginLogoutButton from "./LoginLogoutButton";

const mapStateToProps = state => ({
  isLoggedIn: state.tasks.isLoggedIn,
  editError: state.tasks.editError
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch({ type: LOGOUT })
});

const LoginLogoutButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginLogoutButton);

export default LoginLogoutButtonContainer;
