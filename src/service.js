import axios from "axios";

const BASIC_URL = "https://uxcandy.com/~shapoval/test-task-backend/v2/";
const DEVELOPER_NAME = "?developer=HrushkoO";

class Service {
  async getTasks(data) {
    let url = `${BASIC_URL}${DEVELOPER_NAME}&page=${data.page}`;
    if (data.sortField) {
      url = `${url}&sort_field=${data.sortField}`;
    }
    if (data.sortDirection) {
      url = `${url}&sort_direction=${data.sortDirection}`;
    }
    const response = await axios({
      method: "GET",
      crossDomain: true,
      mimeType: "multipart/form-data",
      contentType: false,
      processData: false,
      dataType: "json",
      url
    });

    if (response.status === 200 && response.data.status === "ok") {
      return response.data.message;
    } else {
      console.error("Can't get tasks");
      return null;
    }
  }

  async createTask(data) {
    const formData = new FormData();
    formData.append("username", data.username);
    formData.append("email", data.email);
    formData.append("text", data.text);

    const response = await axios({
      method: "POST",
      crossDomain: true,
      mimeType: "multipart/form-data",
      contentType: false,
      processData: false,
      data: formData,
      dataType: "json",
      url: `${BASIC_URL}create${DEVELOPER_NAME}`
    });

    if (response.status === 200 && response.data.status === "ok") {
      return response.data.message;
    } else {
      console.error("Can't create task");
      return null;
    }
  }

  async login(data) {
    const formData = new FormData();
    formData.append("username", data.username);
    formData.append("password", data.password);

    const response = await axios({
      method: "POST",
      crossDomain: true,
      mimeType: "multipart/form-data",
      contentType: false,
      processData: false,
      data: formData,
      dataType: "json",
      url: `${BASIC_URL}login${DEVELOPER_NAME}`
    });

    if (response.status === 200) {
      return response.data;
    } else {
      console.error("Can't login");
      return null;
    }
  }

  async editTask(data) {
    const formData = new FormData();
    formData.append("text", data.text);
    formData.append("status", data.status);
    formData.append("token", localStorage.getItem("beejee-token"));

    const response = await axios({
      method: "POST",
      crossDomain: true,
      mimeType: "multipart/form-data",
      contentType: false,
      processData: false,
      data: formData,
      dataType: "json",
      url: `${BASIC_URL}edit/${data.id}${DEVELOPER_NAME}`
    });

    if (response.status === 200) {
      return response.data;
    } else {
      console.error("Can't update task");
      return null;
    }
  }
}

export default new Service();
