export const SET_TASKS = "SET_TASKS";
export const CREATE_TASK = "CREATE_TASK";
export const SET_SHOW_NOTIFICATION = "SET_SHOW_NOTIFICATION";
export const SET_PAGE = "SET_PAGE";
export const SET_SORT_FIELD = "SET_SORT_FIELD";
export const SET_SORT_DIRECTION = "SET_SORT_DIRECTION";
export const LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGOUT = "LOGOUT";
export const EDIT_TASK_SUCCESSFUL = "EDIT_TASK_SUCCESSFUL";
export const EDIT_TASK_FAILED = "EDIT_TASK_FAILED";
