import service from "../../service";
import {
  CREATE_TASK,
  EDIT_TASK_FAILED,
  EDIT_TASK_SUCCESSFUL,
  LOGIN_FAILED,
  LOGIN_SUCCESSFUL,
  SET_TASKS
} from "./actionTypes";

export function getTasks(dispatch) {
  return async data => {
    try {
      const value = await service.getTasks(data);
      dispatch({ type: SET_TASKS, value });
    } catch (error) {
      console.error(error);
    }
  };
}

export function createTask(dispatch) {
  return async data => {
    try {
      const value = await service.createTask(data);
      dispatch({ type: CREATE_TASK, value });
    } catch (error) {
      console.error(error);
    }
  };
}

export function login(dispatch) {
  return async data => {
    try {
      const value = await service.login(data);
      if (value.status === "ok") {
        dispatch({ type: LOGIN_SUCCESSFUL, value: value.message });
      } else {
        dispatch({ type: LOGIN_FAILED });
      }
    } catch (error) {
      console.error(error);
    }
  };
}

export function editTask(dispatch) {
  return async data => {
    try {
      const value = await service.editTask(data);
      if (value.status === "ok") {
        dispatch({ type: EDIT_TASK_SUCCESSFUL });
      } else {
        dispatch({ type: EDIT_TASK_FAILED });
      }
    } catch (error) {
      console.error(error);
    }
  };
}
