import {
  CREATE_TASK,
  EDIT_TASK_FAILED,
  EDIT_TASK_SUCCESSFUL,
  LOGIN_FAILED,
  LOGIN_SUCCESSFUL,
  LOGOUT,
  SET_PAGE,
  SET_SHOW_NOTIFICATION,
  SET_SORT_DIRECTION,
  SET_SORT_FIELD,
  SET_TASKS
} from "../actions/actionTypes";

const initialState = {
  sortField: null,
  sortDirection: null,
  page: 1,
  totalTaskCount: 0,
  tasks: [],
  showNotification: false,
  updateList: true,
  isLoggedIn: !!localStorage.getItem("beejee-token"),
  loginError: false,
  editError: false
};

const tasks = (state = initialState, action) => {
  switch (action.type) {
    case SET_TASKS:
      return {
        ...state,
        tasks: action.value.tasks,
        totalTaskCount: Number(action.value.total_task_count),
        updateList: false
      };
    case CREATE_TASK:
      return {
        ...state,
        showNotification: true,
        updateList: true
      };
    case SET_SHOW_NOTIFICATION:
      return {
        ...state,
        showNotification: action.value
      };
    case SET_PAGE:
      return {
        ...state,
        page: action.value,
        updateList: true
      };
    case SET_SORT_FIELD:
      return {
        ...state,
        sortField: action.value,
        updateList: true
      };
    case SET_SORT_DIRECTION:
      return {
        ...state,
        sortDirection: action.value,
        updateList: true
      };
    case LOGOUT:
      localStorage.clear("beejee-token");
      return {
        ...state,
        isLoggedIn: false
      };
    case LOGIN_SUCCESSFUL: {
      localStorage.setItem("beejee-token", action.value.token);
      return {
        ...state,
        isLoggedIn: true,
        loginError: false,
        editError: false
      };
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        loginError: true
      };
    }
    case EDIT_TASK_SUCCESSFUL: {
      return {
        ...state,
        updateList: true
      };
    }
    case EDIT_TASK_FAILED: {
      localStorage.clear("beejee-token");
      return {
        ...state,
        isLoggedIn: false,
        editError: true
      };
    }
    default:
      return state;
  }
};

export default tasks;
